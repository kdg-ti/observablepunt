# Voorbeeld LogObserver/Punt
## Design Patterns, deel 2
### Java Programmeren 2

## Branches
1. 1_start bevat demo code zonder Observer.
2. 2_observer bevat het basis LogObserver voorbeeld (p44 design patterns deel 1)
3. 3_observer_delegation bevat het LogObserver voorbeeld met delegation (p49 design patterns deel 1)
4. 4_observer_inheritance bevat het LogObserver voorbeeld met overerving (p51 design patterns deel 1)
    De bedoeling en de werking zijn dezelfde als het voorbeeld in de slides, maar de implementatie heeft volgende verschillen:
    - de Observable is een inner klasse van ObserveerbaarPunt
    - de Observable toString verwijst door naar het ObserveerbaarPunt
