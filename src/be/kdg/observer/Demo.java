package be.kdg.observer;

public class Demo {
    public static void main(String[] args) {
        Punt punt = new Punt(1, 2);
	    System.out.println("Start: " + punt);

        punt.verdubbelX();
	    System.out.println("2X: "+ punt);
        punt.verdubbelY();
	    System.out.println("2Y: " + punt);

    }
}
